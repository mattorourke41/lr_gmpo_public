# author: Matt O'Rourke, 10/7/2019
# If you are using these ideas in your research, please cite the paper listed
# in the README. Thank you!

import numpy
np = numpy
import scipy.linalg as LA
import copy
from . import mpo_gen as mpogen

tensordot = np.tensordot
einsum = np.einsum


# This code is meant as an example that closely follows the discussion in
# "A simplified and improved approach to tensor network operators in two
# dimensions" by O'Rourke and Chan. Specifically, following the algorithm in
# the paper, this code will generate the bottom MPO for step 2, the gMPOs
# needed for step 4 (the gMPO is slightly different at each iteration), and the
# vertical MPO that gets applied tensor-by-tensor along the columns in order to
# generate intops for each iteration.
#
# Since this is just an example meant to clarify the construction of the
# long-range pairwise Hamiltonian with non-trivial interaction coefficients, we
# will just show how to construct the tensors for pairwise interactions of
# single operators under a Gaussian interaction potential with exponential
# coefficient "lambda". A set of such operators needs to be generated if one
# wants to fit a different type of interaction potential using Gaussian
# basis functions, as described in the paper. Please note that this
# implementation is meant for clarity, so CPU and memory usage are not optimal
# at all.
#
# If you have questions about the code, please create an issue in this Gitlab
# repo.


# For clarity, the 2D Hamiltonian generated in this file is given by:
# H = \sum_{i<j} A_i B_j exp(-lambda * || \vec{r}_j - \vec{r}_i ||^2)




# This function returns:
# - a bottom MPO of length Lx, which has the conventions (i) mpo[x] returns the
#   4-index MPO tensor for site x (0-based) and (ii) each 4-index MPO tensor
#   has the index convention T[left,right,phys1,phys2].
# - a vertical MPO of length Ly-1, which is the MPO that is applied tensor-by-
#   tensor along each column as the iterations progress to generate/update intops.
#   Each column uses the same MPO tensors, so only a single MPO is returned. The
#   conventions are (i) mpo[y] returns the 4-index MPO tensor for site y (0-based)
#   and (ii) each 4-index mpo tensor has the index convention
#   T[down,up,phys1,phys2].
# - a dictionary of gMPOs. A different gMPO must be applied to each row as the
#   algorithm iterates, so the full set is returned. The conventions are (i)
#   gmpo_dict[ypos] = gmpo, meaning that the keys of the dict are integers and
#   the values are gMPOs. "ypos" is a 0-based integer indexing the row in which
#   the given gMPO should be applied. Given that no gMPO is applied to the
#   bottom row, gmpo_dict[0] will have no value and gmpo_dict[Ly-1] will have
#   the final gMPO. (ii) gmpo[x] yields the 5-index gMPO tensor at site
#   x (0-based) and (iii) the index convention is given in the gmpo_gen()
#   function documentation.
def get_ops(Lx, Ly, A, B, lam, dtype='float'):
    assert type(lam) in [float, np.float64]
    assert type(Lx) in [int, np.int64]
    assert type(Ly) in [int, np.int64]
    d = A.shape[0]
    assert d == A.shape[1]
    assert (d,d) == B.shape
    if Lx < Ly:
        raise ValueError("we need Lx >= Ly for this to work")

    one = np.eye(d, dtype=dtype)
    zero = np.zeros([d,d],dtype=dtype)
    A = np.array(A, dtype=dtype)
    B = np.array(B, dtype=dtype)

    # bottom MPO to be returned for step 2
    bot_mpo = mpogen.build_gauss(Lx, lam, A, B, sthresh=1e-10, dtype=dtype)

    # ref MPO to be used to extract the v, X, and w coefficients. In principle
    # one can use the bot_mpo to do this, but you then need to know the explicit
    # form of A or B. By doing this, we don't need to know.
    ref_mpo = mpogen.build_gauss(Lx, lam, one, one, sthresh=1e-10, dtype=dtype)
    v = np.zeros(Lx, dtype=object)
    w = np.zeros(Lx, dtype=object)
    X = np.zeros(Lx, dtype=object)
    for idx, T in enumerate(ref_mpo):
        if idx == 0:
            w[idx] = [1.0]
            X[idx] = None
            v[idx] = None
        elif idx == Lx-1:
            w[idx] = None
            X[idx] = None
            v[idx] = T[1:-1,0,0,0]
        else:
            v[idx] = T[1:-1, 0, 0, 0]
            X[idx] = T[1:-1, 1:-1, 0, 0]
            w[idx] = T[-1, 1:-1, 0, 0]

    # build the vertical MPO
    vert_mpo = np.zeros(Ly-1,dtype=object)
    for idx in range(Ly-1):
        if idx == 0:
            T = np.zeros([1, len(w[idx])+1, d, d], dtype=dtype)
            T[0,0,:,:] = one
            for i in range(len(w[idx])):
                T[0,i+1,:,:] = w[idx][i] * A
            vert_mpo[idx] = copy.deepcopy(T)

        else:
            T = np.zeros([X[idx].shape[0]+1, len(w[idx])+1, d, d], dtype=dtype)
            T[0,0] = one
            for i in range(len(w[idx])):
                T[0,i+1,:,:] = w[idx][i] * A
                for j in range(X[idx].shape[0]):
                    T[j+1, i+1, :, :] = X[idx][j,i] * one
            vert_mpo[idx] = copy.deepcopy(T)

    # build the gMPO dict
    gmpo_dict = {}
    for idx in range(1,Ly):
        gmpo_dict[idx] = gmpo_gen(Lx, idx, A, B, v, X, w, dtype=dtype)


    return bot_mpo, vert_mpo, gmpo_dict



# Constructs a gMPO of length Lx to be applied on the row "ypos". "ypos"
# indexing starts from 0, so the bottom row of the PEPS is ypos=0 (no gMPO will
# be applied to this row, only the bottom MPO and intops operators).
# The index convention of the gMPO tensors is T[left,q,right,p1,p2], where "q"
# is the index that connects the gMPO to intops.
def gmpo_gen(Lx, ypos, A, B, v, X, w, dtype='float'):
    d = A.shape[0]
    one = np.eye(d, dtype=dtype)
    zero = np.zeros([d,d], dtype=dtype)

    ddown = len(v[ypos]) + 1
    gmpo = np.zeros(Lx,dtype=object)

    # left site
    dr = len(w[0])
    l = np.zeros([1, ddown, 2+2*dr, d, d], dtype=dtype)
    l[0,0,-1,:,:] = one
    for i in range(dr):
        l[0,0,i+1,:,:] = w[0][i] * A
        l[0,0,dr+1+i,:,:] = w[0][i] * B
    for i in range(1,ddown):
        l[0,i,0,:,:] = v[ypos][i-1] * B
    cmat = np.outer(v[ypos],w[0])
    for s in np.ndindex(cmat.shape):
        l[0,1+s[0],1+s[1],:,:] = cmat[s] * one

    gmpo[0] = copy.deepcopy(l)

    # bulk sites
    for xpos in range(1,Lx-1):
        dl = X[xpos].shape[0]
        dr = X[xpos].shape[1]
        T = np.zeros([2+2*dl, ddown, 2+2*dr, d, d], dtype=dtype)
        T[-1,0,-1,:,:] = one
        T[0,0,0,:,:] = one
        for i in range(dr):
            T[-1,0,i+1,:,:] = w[xpos][i] * A
            T[-1,0,dr+1+i,:,:] = w[xpos][i] * B
            for j in range(dl):
                T[j+1,0,0,:,:] = v[xpos][j] * B
                T[j+1,0,i+1,:,:] = X[xpos][j,i] * one
                T[dl+j+1,0,dr+i+1,:,:] = X[xpos][j,i] * one
        for i in range(1,ddown):
            T[-1,i,0,:,:] = v[ypos][i-1] * B
        cmat = np.outer(v[ypos],w[xpos])
        for s in np.ndindex(cmat.shape):
            T[-1, 1+s[0], 1+s[1], :, :] = cmat[s] * one
        cmat = np.outer(v[xpos],v[ypos])
        for s in np.ndindex(cmat.shape):
            T[dl+1+s[0], 1+s[1], 0, :, :] = cmat[s] * one

        gmpo[xpos] = copy.deepcopy(T)


    # right site
    dl = len(v[Lx-1])
    r = np.zeros([2+2*dl, ddown, 1, d, d], dtype=dtype)
    r[0,0,0,:,:] = one
    for i in range(dl):
        r[i+1,0,0,:,:] = v[Lx-1][i] * B
    for i in range(1,ddown):
        r[-1,i,0,:,:] = v[ypos][i-1] * B
    cmat = np.outer(v[Lx-1],v[ypos])
    for s in np.ndindex(cmat.shape):
        r[dl+1+s[0], 1+s[1], 0, :, :] = cmat[s] * one

    gmpo[-1] = copy.deepcopy(r)

    return gmpo
