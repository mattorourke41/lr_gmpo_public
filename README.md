# lr-gmpo-public

A straightforward example implementation of long-range Gaussian gMPOs from the following paper: [arxiv:1911.04592](https://arxiv.org/abs/1911.04592).

If you use this technique in your work, please remember to cite the following reference:

```
@article{o2019simplified,
  title={A simplified and improved approach to tensor network operators in two dimensions},
  author={O'Rourke, Matthew J and Chan, Garnet Kin-Lic},
  journal={arXiv preprint arXiv:1911.04592},
  year={2019}
}
```

compatible with python 2 and python 3
