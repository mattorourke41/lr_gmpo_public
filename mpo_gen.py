# author: Matt O'Rourke, 10/7/2019

import numpy
np = numpy
import scipy.linalg
import copy


# this is a very slow (improper scaling) implementation of the approximate
# MPO construction algorithm from "Sliced Basis DMRG" by Stoudenmire and White,
# PRL (2017). DOI: 10.1103/PhysRevLett.119.046401




# wrapper to the outside world to help build an MPO for the Hamiltonian
# H = \sum_{i<j} A_i B_j exp(-alpha * (j-i)^2)
def build_gauss(L, alpha, A, B, sthresh=1e-10, dtype='float'):
    V = numpy.zeros([L,L], dtype=dtype)
    for i in range(L):
        for j in range(L):
            V[i,j] = np.exp(-alpha * np.abs(i-j)**2)

    return build(V, A, B, sthresh, dtype=dtype)



# given a potential matrix V and operators A,B for the Hamiltonian
# H = \sum_{i<j} V_{ij} A_i B_j, generate the compressed MPO.
# MPO tensors have the following index convention: T[l,r,phys,phys].
# The variable names in this function are meant to closely correspond with the
# expressions in the Stoudenmire + White paper.
def build(V, A, B, sthresh=1e-8, dtype='float'):
    L = V.shape[0]
    assert V.shape[1] == L
    d = A.shape[0]
    assert A.shape[1] == d
    assert (d,d) == B.shape

    one = np.eye(d, dtype=dtype)
    zero = np.zeros((d,d), dtype=dtype)

    ret = np.zeros(L,dtype=object)

    # first site
    T = np.zeros([1,3],dtype=object)
    T[0,0] = zero
    T[0,1] = A
    T[0,2] = one
    ret[0] = copy.deepcopy(T)

    # bulk sites
    Uprev = np.array([[1.0]], dtype=dtype) # uprev holds the svd U matrix from the previous iter
    for i in range(1,L):
        Vp = V[:(i+1),i:]
        u,s,w = scipy.linalg.svd(Vp,full_matrices=False,lapack_driver='gesvd')
        num = np.sum(s > sthresh)
        u = u[:,:num]
        s = s[:num]
        w = w[:num,:]
        pUprev = np.zeros([Uprev.shape[0]+1, Uprev.shape[1]+1], dtype=dtype)
        pUprev[:-1,:-1] = Uprev
        pUprev[-1,-1] = 1.0
        Xp = np.dot(pUprev.T, u)
        Sig = Xp.dot(np.diag(s)).dot(w)

        T = np.zeros([Xp.shape[0]+1,Xp.shape[1]+2],dtype=object)
        for idx in np.ndindex(T.shape):
            T[idx] = zero
        T[0,0] = one
        T[-1,-1] = one
        for j in range(1,T.shape[0]-1):
            T[j,0] = Sig[j-1,0] * B
        for j in range(1,T.shape[0]):
            for k in range(1,T.shape[1]-1):
                if j < T.shape[0]-1:
                    T[j,k] = Xp[j-1,k-1] * one
                else:
                    assert j == T.shape[0]-1
                    T[j,k] = Xp[j-1,k-1] * A

        ret[i] = copy.deepcopy(T)
        Uprev = u

    # final site
    s = ret[-1].shape
    ret[-1] = ret[-1][:,0].reshape(s[0],1)

    # expose physical indices
    mpo = twoToFour(ret, dtype=dtype)
    return mpo


# helper function to convert matrix-valued matrices to true 4-index MPO tensors
def twoToFour(mpo, dtype='float'):
    L = len(mpo)
    ret = np.zeros(L,dtype=object)
    for i in range(L):
        (dl,dr) = mpo[i].shape
        dp = mpo[i][0,0].shape[0]
        T = np.zeros([dl,dr,dp,dp], dtype=dtype)
        for idx in np.ndindex(mpo[i].shape):
            for idx2 in np.ndindex(mpo[i][idx].shape):
                tidx = idx + idx2
                T[tidx] = mpo[i][idx][idx2]
                ret[i] = copy.deepcopy(T)
    return ret
